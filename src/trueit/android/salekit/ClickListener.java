package trueit.android.salekit;

import java.util.HashMap;

public interface ClickListener {
	void onClick(int id, HashMap<String, Object> hData);
}
