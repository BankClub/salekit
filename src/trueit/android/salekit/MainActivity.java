package trueit.android.salekit;


import java.util.HashMap;
import java.util.Vector;
import java.util.zip.Inflater;

import trueit.android.salekit.data.SaleKitDB;
import trueit.android.salekit.fragment.DetailFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.os.Build;

public class MainActivity extends FragmentActivity implements ClickListener{
	private int index_promotion;
	
	public static final String PACKAGE = "package";
	public static final String DETAIL = "detail";
	
	public static final int INT_TOL = 1;
	public static final int INT_TVS = 2;
	public static final int INT_TMV = 3;
//	public static final int BILL = 13;
	
	public static final String TOL = "1";
	public static final String TVS = "2";
	public static final String TMV = "3";
	
//	public Vector<HashMap<String, String>> packageSook2 = new Vector<HashMap<String, String>>();
//	public Vector<HashMap<String, String>> packageSook3 = new Vector<HashMap<String, String>>();
	
	public Vector<HashMap<String, String>> vPackage;
	private int index_package_select = 0;
	
	
	private LinearLayout layout_menu;
	private LinearLayout layout_body;
	
	private static MainActivity main_activity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		main_activity = this;
			
		index_promotion = getIntent().getIntExtra(HomeActivity.MENU_INDEX, HomeActivity.SOOK2);
		layout_menu = (LinearLayout) findViewById(R.id.main_layout_menu);
		layout_menu.removeAllViews();
		
		LinearLayout home_element = (LinearLayout) findViewById(R.id.menu_layout_home);	
		home_element.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		vPackage = SaleKitDB.getInstance().getPackage(""+index_promotion);
		initPackage(vPackage);
			
		
		setTarget(0);
			
	}

	
	public static MainActivity getInstance(){
		return main_activity;
	}
	
	public int getPromotionId(){
		return index_promotion;
	}
	
	private void setTarget(int index){
		for(int i=0; i<layout_menu.getChildCount(); i++){
			View element_package = layout_menu.getChildAt(i);
			TextView str_package = (TextView) element_package.findViewById(R.id.element_menu_txt_menu);
	
			if (i == index) {
				str_package.setTextColor(Color.GREEN);
				clearFragment();
				goPackage(PACKAGE, index);
			} else {
				str_package.setTextColor(Color.WHITE);
			}
		}
	}
	
	private void initPackage(Vector<HashMap<String, String>> vPackage){
		for(int i=0; i<vPackage.size(); i++){
			HashMap<String, String> hData = vPackage.elementAt(i);
			
			View element_package = getLayoutInflater().inflate(R.layout.element_menu, layout_menu, false);
			ImageView imv_package = (ImageView) element_package.findViewById(R.id.element_menu_imv_menu);
			imv_package.setVisibility(View.GONE);
			
			
			TextView str_package = (TextView) element_package.findViewById(R.id.element_menu_txt_menu);
			str_package.setVisibility(View.VISIBLE);
			str_package.setText(hData.get(SaleKitDB.PACKAGE_PRICE));
			
			final int f_index = i;
			element_package.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
//					clearFragment();
//					goPackage(PACKAGE, f_index);
					index_package_select = f_index;
					setTarget(f_index);
				}
			});
			

			
			layout_menu.addView(element_package);
		}
		
	}
	
	public void setPackageSelect(String package_price_id){
		for(int i=0; i<vPackage.size(); i++){
			HashMap<String, String> hPackage = vPackage.elementAt(i);
			if(hPackage.get(SaleKitDB.PACKAGE_PRICE_ID) != null && package_price_id.equals(hPackage.get(SaleKitDB.PACKAGE_PRICE_ID))){
				this.index_package_select = i;
			}
		}
		
//		clearFragment();
//		goPackage(PACKAGE, index_package_select);
		setTarget(index_package_select);
	}
	
	@Override
	public void onBackPressed() {
		FragmentManager fm = getSupportFragmentManager();
		if(fm.getBackStackEntryCount() == 1)
			finish();
		super.onBackPressed();
	}
	
	public void clearFragment(){
		FragmentManager fm = getSupportFragmentManager();
//		FragmentTransaction ft = fm.beginTransaction();
		fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	}

	private void goPackage(String type, int index){
		Bundle arguments = new Bundle();
		arguments.putString(DetailFragment.PAGE, type);
		arguments.putInt(DetailFragment.INDEX, index);
		DetailFragment fragment = new DetailFragment();
		fragment.setClickListener(this);
		fragment.setArguments(arguments);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
		ft.replace(R.id.main_layout_body, fragment, type).addToBackStack(type).commit();

	}

	@Override
	public void onClick(int id, HashMap<String, Object> hData) {
		// TODO Auto-generated method stub
		switch (id) {
		case INT_TVS: //tvs
			break;
		case INT_TMV:
			break;
		case INT_TOL:
			break;
		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == RESULT_OK)
		{
			if(requestCode == MainActivity.INT_TVS || requestCode == MainActivity.INT_TOL)
			{
				String sPackage = data.getStringExtra(TVSDetailActivity.KEY_PACKAGE);
				setPackageSelect(sPackage);
			}
		}
		
	}
	

}










