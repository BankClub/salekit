package trueit.android.salekit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class HomeActivity extends Activity{
	public static final int SOOK2 = 1;
	public static final int SOOK3 = 2;
	
	public static final String MENU_INDEX = "index";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home);
		
		ImageView sook2 = (ImageView) findViewById(R.id.home_imv_sook2);
		sook2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent_main = new Intent(HomeActivity.this, MainActivity.class);
				intent_main.putExtra(MENU_INDEX, SOOK2);
				startActivity(intent_main);
			}
		});
		
		ImageView sook3 = (ImageView) findViewById(R.id.home_imv_sook3);
		sook3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent intent_main = new Intent(HomeActivity.this, MainActivity.class);
//				intent_main.putExtra(MENU_INDEX, SOOK3);
//				startActivity(intent_main);
			}
		});
		
		
		super.onCreate(savedInstanceState);
	}
}
