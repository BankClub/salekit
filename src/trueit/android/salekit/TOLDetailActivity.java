package trueit.android.salekit;

import java.util.HashMap;
import java.util.Vector;

import trueit.android.salekit.data.SaleKitDB;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TOLDetailActivity extends Activity{
	public static final String KEY_PRODUCT = "product";
	public static final String KEY_PROMOTION = "promotion";
	public static final String KEY_TVS_PRODUCT_PRICE = "tvs_product_price";
	public static final String KEY_TOL_PRODUCT_PRICE = "tol_product_price";
	public static final String KEY_TMV_PRODUCT_PRICE = "tmv_product_price";
	
	public static final String KEY_PACKAGE = "package";
	private Vector<HashMap<String, String>> vProductPrice;
	private LinearLayout layout_body;
	private ImageView imv_tol_detail;
	
	private int start_product = 0;
	private int select_product = 0;
	private String product_id = MainActivity.TOL;
	private String promotion_id = ""+HomeActivity.SOOK2;
	private String tol_product_price_id;
	private String tvs_product_price_id;
	private String tmv_product_price_id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tol_detail);
		
		layout_body = (LinearLayout) findViewById(R.id.tol_layout_body);
		layout_body.removeAllViews();
		
		imv_tol_detail = (ImageView) findViewById(R.id.tol_imv_package_detail);
		
		try{
			product_id = getIntent().getStringExtra(KEY_PRODUCT);
			promotion_id = getIntent().getStringExtra(KEY_PROMOTION);
			tvs_product_price_id = getIntent().getStringExtra(KEY_TVS_PRODUCT_PRICE);
			tol_product_price_id = getIntent().getStringExtra(KEY_TOL_PRODUCT_PRICE);
			tmv_product_price_id = getIntent().getStringExtra(KEY_TMV_PRODUCT_PRICE);
		}
		catch(Exception e){}
		
		vProductPrice = SaleKitDB.getInstance().getProductPrice(product_id, promotion_id);
		setProduct();
		
		TextView bnt_cancel = (TextView) findViewById(R.id.detail_txt_cancel);
		bnt_cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		
		TextView bnt_ok = (TextView) findViewById(R.id.detail_txt_ok);
		bnt_ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(start_product != select_product){
					HashMap<String, String> hProductPrice = vProductPrice.elementAt(select_product);
					String sPackage = "";
					
					Vector<String> vProductPriceId = new Vector<String>();
					vProductPriceId.addElement(hProductPrice.get(SaleKitDB.PRODUCT_PRICE_ID));
					if(tvs_product_price_id != null){
						vProductPriceId.addElement(tvs_product_price_id);
					}
					
					if(tmv_product_price_id != null){
						vProductPriceId.addElement(tmv_product_price_id);
					}
					
					if(vProductPriceId.size() > 1){
						
						
						String[] arr_product_price_id = new String[vProductPriceId.size()];
						for(int i=0; i<vProductPriceId.size(); i++){
							arr_product_price_id[i] = vProductPriceId.elementAt(i);
						}
						Vector<HashMap<String, String>> vPackagePrice = SaleKitDB.getInstance().getPackagePrice(arr_product_price_id, promotion_id);
						if(vPackagePrice.size() == 0){
							vPackagePrice = SaleKitDB.getInstance().getPackagePrice(hProductPrice.get(SaleKitDB.PRODUCT_PRICE_ID), promotion_id);
							
							HashMap<String, String> hPackagePrice = vPackagePrice.elementAt(0);
									
							sPackage = hPackagePrice.get(SaleKitDB.PACKAGE_PRICE_ID);
						}
						else{
							HashMap<String, String> hPackagePrice = vPackagePrice.elementAt(0);
							sPackage = hPackagePrice.get(SaleKitDB.PACKAGE_PRICE_ID);
						}
					}
					else{
						Vector<HashMap<String, String>> vPackagePrice = SaleKitDB.getInstance().getPackagePrice(hProductPrice.get(SaleKitDB.PRODUCT_PRICE_ID), promotion_id);
						
						HashMap<String, String> hPackagePrice = vPackagePrice.elementAt(0);
								
						sPackage = hPackagePrice.get(SaleKitDB.PACKAGE_PRICE_ID);					
					}
									
					
					Intent intent = getIntent();
					if(intent == null)
					{
						intent = new Intent();
					}
					
					intent.putExtra(KEY_PACKAGE, sPackage);
					
					
					setResult(RESULT_OK, intent);
				}
		
				finish();
			}
		});
	}
	
	
	
	private void setProduct(){
		for(int i=0; i<vProductPrice.size(); i++){
			HashMap<String, String> hProductPrice = vProductPrice.elementAt(i);
			
			String product_price_link = hProductPrice.get(SaleKitDB.PRODUCT_PRICE_LINK);
			product_price_link = "tol_"+product_price_link.replace(".png", "");
			int restID = getResources().getIdentifier(product_price_link, "drawable", getPackageName());
			
			String product_price_name = hProductPrice.get(SaleKitDB.PRODUCT_PRICE_NAME);
			
			TextView txt_product = new TextView(this);
			txt_product.setPadding(10, 0, 0, 10);
			txt_product.setTextSize(30);
			txt_product.setText(product_price_name);
			
			
			layout_body.addView(txt_product);
			
			String product_price_id = hProductPrice.get(SaleKitDB.PRODUCT_PRICE_ID);
			if(this.tol_product_price_id.equals(product_price_id)){
				
				imv_tol_detail.setImageResource(restID);
				
				start_product = i;
				select_product = i;
			}
			
			final int f_index = i;
			final int f_restID = restID;
			txt_product.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					select_product = f_index;
					HashMap<String, String> hProductPrice = vProductPrice.elementAt(f_index);

					imv_tol_detail.setImageResource(f_restID);
				}
			});
			
		}
	}
}
