package trueit.android.salekit;

import android.app.Application;
import android.content.Context;

public class SaleKitApp extends Application{
	
	private static Context context;
	@Override
	public void onCreate() {
		context = this;
		
		super.onCreate();
	}
	
	public static Context getContext(){
		return context;
	}
}
