package trueit.android.salekit.fragment;

import java.util.HashMap;
import java.util.Vector;

import trueit.android.salekit.ClickListener;
import trueit.android.salekit.TOLDetailActivity;
import trueit.android.salekit.TVSDetailActivity;
import trueit.android.salekit.HomeActivity;
import trueit.android.salekit.MainActivity;
import trueit.android.salekit.R;
import trueit.android.salekit.data.SaleKitDB;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class DetailFragment extends Fragment{
	private int width = 0;
	private int height = 0;
	private int real_img_w = 0;
	private int real_img_h = 0;
	private int startX = 0;
	private int startY = 0;
	
	
	private ImageView imv_screen;
	private FrameLayout layout_body;
	
	private Activity activity;
	
	public static final String PAGE = "page";
	public static final String INDEX = "index";
	
	private String page;
	private int index;
	
	private ClickListener mClickListener;
	
	
	private String product_id = MainActivity.TOL;
	private String product_price_id;
	
	private int layout_w = 265;
	private int layout_h = 310;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(PAGE)) {
			page = getArguments().getString(PAGE);	
			index = getArguments().getInt(INDEX);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View detail_view = inflater.inflate(R.layout.fragment_detail, container, false);
		
		layout_body = (FrameLayout) detail_view.findViewById(R.id.detail_layout_detail);
		layout_body.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			
			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				width = layout_body.getWidth();
				height = layout_body.getHeight();
				
				init();
				layout_body.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}
		});
		
		
		imv_screen = (ImageView) detail_view.findViewById(R.id.detail_imv_screen1);	

		return detail_view;
	}
	
	
	private void init(){
		HashMap<String, String> hData = new HashMap<String, String>();
		Vector<HashMap<String, String>> vProductPrice = new Vector<HashMap<String,String>>();
		final HashMap<String, String> hProductPriceID = new HashMap<String, String>();
		
		Bitmap real_img = null;
		if(page.equals(MainActivity.PACKAGE)){
			hData = MainActivity.getInstance().vPackage.elementAt(index);
			vProductPrice = SaleKitDB.getInstance().getProductPrice(hData.get(SaleKitDB.PACKAGE_PRICE_ID));
			
			String mDrawableName = hData.get(SaleKitDB.PACKAGE_PRICE_LINK);
			mDrawableName = mDrawableName.replace(".png", "");
			int restID = getResources().getIdentifier(mDrawableName, "drawable", activity.getPackageName());
			real_img = BitmapFactory.decodeResource(getResources(), restID);
			
//			if(MainActivity.getInstance().index_promotion == HomeActivity.SOOK2){
//
//
//			}
//			else if(MainActivity.getInstance().index_promotion == HomeActivity.SOOK3){
//
//			}
			
			for(int i=0; i<vProductPrice.size(); i++){
				HashMap<String, String> hProductPrice = vProductPrice.elementAt(i);
				String product_id = hProductPrice.get(SaleKitDB.PRODUCT_ID);
				hProductPriceID.put(product_id, hProductPrice.get(SaleKitDB.PRODUCT_PRICE_ID));
			}
		}

		
		real_img_w = real_img.getWidth();
		real_img_h = real_img.getHeight();
		
		int dif_w = real_img_w - width;
		int dif_h = real_img_h - height;
		
		Bitmap use_img = null;
		if(dif_w > dif_h){
			use_img = reImage(real_img, width, -1);
			startY = (height-use_img.getHeight())/2;
		}
		else{
			use_img = reImage(real_img, -1, height);
			startX = (width-use_img.getWidth())/2;
		}
		
		
		imv_screen.setImageBitmap(use_img);
		
		layout_w = (layout_w*use_img.getWidth())/real_img_w;
		layout_h = (layout_h*use_img.getHeight())/real_img_h;
		
		if(page.equals(MainActivity.PACKAGE)){
			
			
			for(int i=0; i<vProductPrice.size(); i++){
				final HashMap<String, String> hProductPrice = vProductPrice.elementAt(i);
		
				if(hProductPrice.get(SaleKitDB.PRODUCT_PRICE_X) != null && hProductPrice.get(SaleKitDB.PRODUCT_PRICE_Y) != null){
					LinearLayout layout_click = new LinearLayout(activity);
					layout_click.setLayoutParams(new LayoutParams(layout_w, layout_w));
					
					int[] xy = getXY(Integer.parseInt(hProductPrice.get(SaleKitDB.PRODUCT_PRICE_X)), Integer.parseInt(hProductPrice.get(SaleKitDB.PRODUCT_PRICE_Y)));
					layout_click.setX(xy[0]);
					layout_click.setY(xy[1]);
					layout_click.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
//							Toast.makeText(activity, hProductPrice.get(SaleKitDB.PRODUCT_ID), Toast.LENGTH_SHORT).show();
							String product_id = hProductPrice.get(SaleKitDB.PRODUCT_ID);
							if(product_id.equals(MainActivity.TVS)){
								Intent dialog_tvs = new Intent(activity, TVSDetailActivity.class);
								dialog_tvs.putExtra(TVSDetailActivity.KEY_PROMOTION, ""+ MainActivity.getInstance().getPromotionId());
								dialog_tvs.putExtra(TVSDetailActivity.KEY_PRODUCT, hProductPrice.get(SaleKitDB.PRODUCT_ID));
								dialog_tvs.putExtra(TVSDetailActivity.KEY_TVS_PRODUCT_PRICE, hProductPrice.get(SaleKitDB.PRODUCT_PRICE_ID));
								
								dialog_tvs.putExtra(TVSDetailActivity.KEY_TOL_PRODUCT_PRICE, hProductPriceID.get(MainActivity.TOL));
								dialog_tvs.putExtra(TVSDetailActivity.KEY_TMV_PRODUCT_PRICE, hProductPriceID.get(MainActivity.TMV));
								
								activity.startActivityForResult(dialog_tvs, MainActivity.INT_TVS);
							}
							else if(product_id.equals(MainActivity.TOL)){
								Intent dialog_tol = new Intent(activity, TOLDetailActivity.class);
								dialog_tol.putExtra(TOLDetailActivity.KEY_PROMOTION, ""+ MainActivity.getInstance().getPromotionId());
								dialog_tol.putExtra(TOLDetailActivity.KEY_PRODUCT, hProductPrice.get(SaleKitDB.PRODUCT_ID));
								dialog_tol.putExtra(TOLDetailActivity.KEY_TOL_PRODUCT_PRICE, hProductPrice.get(SaleKitDB.PRODUCT_PRICE_ID));
								
								dialog_tol.putExtra(TVSDetailActivity.KEY_TVS_PRODUCT_PRICE, hProductPriceID.get(MainActivity.TVS));
								dialog_tol.putExtra(TVSDetailActivity.KEY_TMV_PRODUCT_PRICE, hProductPriceID.get(MainActivity.TMV));
								
								activity.startActivityForResult(dialog_tol, MainActivity.INT_TOL);
							}
						}
					});
					layout_body.addView(layout_click);
				}
			}
			

		}
		
	}
	
	private int[] getXY(int x, int y){
		int[] xy = new int[2];
		xy[0] = (x*width)/real_img_w + startX;
		xy[1] = (y*height)/real_img_h + startY;
		
		return xy;
	}
	

	
	private Bitmap reImage(Bitmap img, int new_width, int new_height){
        int width = img.getWidth();
        int height = img.getHeight();
        int newWidth = 0;
        int newHeight = 0;
        
        if(new_height == -1){
        	newWidth = new_width;
        	newHeight = (newWidth*height)/width;
        }
        else if(new_width == -1){
        	newHeight = new_height;
        	newWidth = (newHeight*width)/height;
        }
        else{
        	newWidth = new_width;
        	newHeight = (newWidth*height)/width;
        }
        
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height; 
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight); 
        Bitmap reImage = Bitmap.createBitmap(img, 0, 0, width, height, matrix, true); 	
        
        return reImage;
	}
	
	@Override
	public void onAttach(Activity activity) {
		this.activity = activity;
		super.onAttach(activity);
	}
	
	public void setClickListener(ClickListener mClickListener){
		this.mClickListener = mClickListener;
	}
}
