package trueit.android.salekit.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Vector;

import trueit.android.salekit.SaleKitApp;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;



public class SaleKitDB {
	private static SaleKitDB salekitDatabase;
	private Context context = null;
	private static DataBaseHelper dbHelper = null;
	
//    private static String dbPath = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/data/" + AgentChat.getContext().getPackageName() + "/databases/";
    private static final String dbName = "salekit.sqlite"; 
    
    private static final String PRODUCT_TB = "product_tb";
    private static final String PROMOTION_TB = "promotion_TB";
    private static final String PRODUCT_PRICE_TB = "product_price_tb";
    private static final String PACKAGE_PRICE_TB = "package_price_tb";
    private static final String PROMOTION_M_PRODUCT_TB = "promotion_M_prduct_tb";
    private static final String PROMOITON_M_PRODUCTPRICE_TB = "promotion_M_product_price_tb";
    private static final String PRODUCTPRICE_M_PACKAGEPRICE_TB = "product_price_M_package_price_tb";
    
    public static final String PROMOTION_ID = "promotion_id";
    public static final String PROMOTION_NAME = "prmotion_name";
    public static final String PROMOTION_LINK = "promotion_link";
    
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    
    public static final String PRODUCT_PRICE_ID = "product_price_id";
    public static final String PRODUCT_PRICE_NAME = "product_price_name";
    public static final String PRODUCT_PRICE = "product_price";
    public static final String PRODUCT_PRICE_OPTION = "product_option";
    public static final String PRODUCT_PRICE_LINK = "product_link";
    
    public static final String PACKAGE_PRICE_ID = "package_price_id";
    public static final String PACKAGE_PRICE_NAME = "package_price_name";
    public static final String PACKAGE_PRICE_LINK = "package_link";
    public static final String PACKAGE_PRICE = "package_price";
    
    public static final String PRODUCT_PRICE_X = "product_price_x";
    public static final String PRODUCT_PRICE_Y = "product_price_y";
    public static final String PRODUCT_PRICE_W = "product_price_w";
    public static final String PRODUCT_PRICE_H = "product_price_h";
    
	private SaleKitDB(Context context) throws IOException {
		this.context = context;
		
		if (dbHelper == null || !dbHelper.isOpen()) {
//			dbPath = Environment.getDataDirectory() + "/data/" + AgentChat.getContext().getPackageName()+ "/databases/";
			dbHelper = new DataBaseHelper(context, dbName);
			dbHelper.createDataBase();
			dbHelper.openDataBase();
		}
	}
	
	public void reCreateDataBase(){
		dbHelper.reCopyDataBase();
	}
	
	public static void closeDatabase(){
		dbHelper.close();
		dbHelper = null;
		
		salekitDatabase = null;
	}
	
	public static SaleKitDB getInstance(){
		if(salekitDatabase == null){
			try{salekitDatabase = new SaleKitDB(SaleKitApp.getContext());}catch(Exception e){Log.w("SaleKitDB", "Not new SaleKitDB");}
		}
		return salekitDatabase;
	}    
    
    
    
    
	
	public Vector<HashMap<String, String>> getPackage(String promotion_id){
		Vector<HashMap<String, String>> vPackage = null;
		
		Cursor c = dbHelper.query("SELECT * FROM " + PACKAGE_PRICE_TB + " WHERE " + PROMOTION_ID + "='" + promotion_id + "' ORDER BY CAST(package_price AS INT) ASC");
	
		vPackage = convertCursor2List(c);
		
		if(c != null)c.close();
		c = null;
		
		return vPackage;
	}
	
	
	public Vector<HashMap<String, String>> getPackagePrice(String[] arr_product_price_id, String promotion_id){
		Vector<HashMap<String, String>> vPackagePrice = null;
		
//		SELECT pkp.* FROM package_price_tb pkp 
//		LEFT JOIN product_price_M_package_price_tb pdppkp ON pkp.package_price_id == pdppkp.package_price_id 
//		WHERE (pdppkp.product_price_id='13' OR pdppkp.product_price_id='21') AND pkp.promotion_id='1'
//                Group by pdppkp.package_price_id
//                HAVING (COUNT(pdppkp.package_price_id) == 2)
//		ORDER BY CAST(pkp.package_price AS INT) ASC
		
		String cmd = "SELECT pkp.* FROM package_price_tb pkp "+
				  "LEFT JOIN product_price_M_package_price_tb pdppkp ON pkp.package_price_id == pdppkp.package_price_id "+
				  "WHERE ";
				  
				  for(int i=0; i<arr_product_price_id.length; i++){
					  if(i == 0)
						  cmd += "(pdppkp.product_price_id='"+ arr_product_price_id[i]+ "' ";
					  else if(i == arr_product_price_id.length-1)
						  cmd += "OR pdppkp.product_price_id='"+ arr_product_price_id[i]+ "') ";				  
					  else
						  cmd += "OR pdppkp.product_price_id='"+ arr_product_price_id[i]+ "' ";
				  }
				  
				  cmd += "AND pkp.promotion_id='"+ promotion_id + "' "+
				  "Group by pdppkp.package_price_id "+
				  "HAVING (COUNT(pdppkp.package_price_id) == " + arr_product_price_id.length+ ") "+
				  
				  "ORDER BY CAST(pkp.package_price AS INT) ASC";
				  
		Cursor c = dbHelper.query(cmd);

		vPackagePrice = convertCursor2List(c);
		
		if(c != null)c.close();
		c = null;
		
		return vPackagePrice;
	}
	
	public Vector<HashMap<String, String>> getPackagePrice(String product_price_id, String promotion_id){
		Vector<HashMap<String, String>> vPackagePrice = null;
		
//		SELECT pkp.* FROM package_price_tb pkp 
//		LEFT JOIN product_price_M_package_price_tb pdppkp ON pkp.package_price_id == pdppkp.package_price_id 
//		WHERE pdppkp.product_price_id='13' AND pkp.promotion_id='1'
//		ORDER BY CAST(pkp.package_price AS INT) ASC

		Cursor c = dbHelper.query("SELECT pkp.* FROM package_price_tb pkp "+
								  "LEFT JOIN product_price_M_package_price_tb pdppkp ON pkp.package_price_id == pdppkp.package_price_id "+
								  "WHERE pdppkp.product_price_id='"+ product_price_id+ "' AND pkp.promotion_id='"+ promotion_id + "' "+
								  "ORDER BY CAST(pkp.package_price AS INT) ASC");
		
		vPackagePrice = convertCursor2List(c);
		
		if(c != null)c.close();
		c = null;
		
		return vPackagePrice;
	}
	
	//from PackageID
	public Vector<HashMap<String, String>> getProductPrice(String package_id){
		Vector<HashMap<String, String>> vProductPrice = null;
		
//		Cursor c = dbHelper.query("SELECT * FROM " + PRODUCTPRICE_M_PACKAGEPRICE_TB + " WHERE " + PACKAGE_PRICE_ID + "='" +package_id+"'");
		Cursor c = dbHelper.query("SELECT * FROM " + PRODUCTPRICE_M_PACKAGEPRICE_TB + " pdppkp "+
								  "LEFT JOIN " + PRODUCT_PRICE_TB + " pdp ON pdppkp.product_price_id == pdp.product_price_id "+
								  "WHERE pdppkp.package_price_id == '"+ package_id +"' ORDER BY pdp.product_id ASC");
		
		vProductPrice = convertCursor2List(c);
		
		if(c != null)c.close();
		c = null;
		
		return vProductPrice;
	}
	
	
	public Vector<HashMap<String, String>> getProductPrice(String product_id, String promotion_id){
		Vector<HashMap<String, String>> vProductPrice = null;
		
//		SELECT pdp.*,pmpdp.* FROM product_price_tb pdp 
//		LEFT JOIN promotion_M_product_price_tb pmpdp ON pdp.product_price_id == pmpdp.product_price_id 
//		WHERE pdp.product_id == '2' AND pmpdp.promotion_id == '1' AND pmpdp.product_option == 'Y'
//		ORDER BY CAST(pdp.product_price AS INT) ASC
		
		Cursor c = dbHelper.query("SELECT pdp.*,pmpdp.* FROM "+PRODUCT_PRICE_TB+" pdp "+
								  "LEFT JOIN promotion_M_product_price_tb pmpdp ON pdp.product_price_id == pmpdp.product_price_id "+
								  "WHERE pdp.product_id == '" + product_id +"' AND pmpdp.promotion_id == '"+ promotion_id + "' AND pmpdp.product_option == 'Y' "+
								  "ORDER BY CAST(pdp.product_price AS INT) ASC");
								  
		vProductPrice = convertCursor2List(c);
		
		if(c != null)c.close();
		c = null;
		
		return vProductPrice;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	private Vector<HashMap<String, String>> convertCursor2List(Cursor c) { 
		
		Vector<HashMap<String, String>> result = null;
		if(c != null) {
			
			result = new Vector<HashMap<String, String>>();
			if(c.moveToFirst()) {
				
				while(!c.isAfterLast()) {
					
					String[] columnName = c.getColumnNames();
					HashMap<String, String> hm = new HashMap<String, String>();
					for(int j=0; j<columnName.length; j++) {				
						try{hm.put(columnName[j], c.getString(c.getColumnIndexOrThrow(columnName[j])));}catch(Exception e){}				
					}

					result.add(hm);
					c.moveToNext();
				}
			}
		}
		return result;
	}
    
    
  //----------------------------------------------------------------------------------------------------
	
  	public class DataBaseHelper extends SQLiteOpenHelper {
//  	    private String dbPath = "";
  	    private String dbName = ""; 
  	    private SQLiteDatabase database = null; 
  	    private final Context context;
  	 
  	    public DataBaseHelper(Context context, String dbName) {
  	    	super(context, dbName, null, 1);
//  	    	this.dbPath =  dbPath;
  	    	this.dbName = dbName;
  	        this.context = context;
  	    }	

  	    protected boolean isOpen() {
  	    	if (database != null && database.isOpen()) 
  	    		return true;
  	    	else 
  	    		return false;
  	    }
  	    
  	    protected void deleteDatabase() {
  			try {
  				if (database != null) {
  					database.close();
  				}
  				
  				String fullPath = context.getDatabasePath(dbName).getAbsolutePath();
  				
  				File fconn = new File(fullPath);
  				
  				if (fconn.exists()){
  					fconn.delete();
  				}
  			}
  			catch(Exception e) {
  				// do something
  			}
  		}
  	    
  	    protected void createDataBase() throws IOException {
  	    	if (!checkDataBase()) {
  	        	
  	    		getReadableDatabase();
  	        	try {
  	    			copyDataBase();
//  	        		createDatebase();
  	    		} 
  	        	catch (Exception e) {
  	        		// code here
  	        	}
  	    	}
  	    }

//  	    private void createDatebase(){
//  	        String fullPath = context.getDatabasePath(dbName).getAbsolutePath();
//  	        try {   	
//  	        	database = this.getWritableDatabase();
//  	        	database = SQLiteDatabase.openDatabase(fullPath, null, SQLiteDatabase.OPEN_READWRITE);
////  	        	database = SQLiteDatabase.openDatabase(fullPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//  	        	
//  	        	
//  	        	String create_message = "CREATE TABLE Message (id_ VARCHAR(50) NOT NULL ,ref_id VARCHAR(50),                               type VARCHAR(15), from_id VARCHAR(50), from_name VARCHAR(50), to_id VARCHAR(50),                                text VARCHAR(200),link VARCHAR(50),link_type VARCHAR(15),                               read INTEGER DEFAULT 0, send INTEGER DEFAULT 0,cDate DOUBLE, uDate DOUBLE, seq_id DOUBLE DEFAULT 0, \"to_name\" TEXT, \"owner\" TEXT, \"friend_read\" TEXT DEFAULT (0), \"thumb_url\" TEXT )";
//  	        	database.execSQL(create_message);
//  	        	
//  	        	String create_stream = "CREATE TABLE Stream (id_ VARCHAR(50) NOT NULL ,ref_id VARCHAR(50),                               type VARCHAR(15), from_id VARCHAR(50), from_name VARCHAR(50), to_id VARCHAR(50),                                title VARCHAR(50), detail VARCHAR(200),                               cDate DOUBLE, uDate DOUBLE, unreadCount INTEGER DEFAULT 0, \"to_name\" TEXT, \"owner\" TEXT)";
//  	        	database.execSQL(create_stream);
//  	        	
//  	        	String create_group_match = "CREATE TABLE \"GroupMatch\" (\"user_id\" TEXT, \"group_id\" TEXT)";
//  	        	database.execSQL(create_group_match);
//  	        	
//  	        	String create_notification = "CREATE TABLE \"notification_tb\" (\"user_id\" TEXT, \"notification\" CHAR(1))";
//  	        	database.execSQL(create_notification);
//  	        	
//  	        	String crete_contact = "CREATE TABLE \"ChatContactTB\" (\"empId\" TEXT,\"fullName\" ,\"position\" ,\"company\" ,\"telNumber\" ,\"email\" ,\"imageURL\"  DEFAULT (null) ,\"statusId\" ,\"statusText\" ,\"salutaion_en\" , \"chat\")";
//  	        	database.execSQL(crete_contact);
//  	        	
//  	        	String create_group = "CREATE TABLE \"GroupTB\" (\"id\" TEXT, \"g_name\" TEXT, \"owner\" TEXT, \"link\" TEXT, \"desc\" TEXT, \"noti\" TEXT,  \"uDate\" DOUBLE)";
//  	        	database.execSQL(create_group);
//  	        	
//  	        	if(database != null)
//  	        		database.close();
//  	        	
//  	        } catch (SQLException e) {
//  	            e.printStackTrace();
//  	        }
//  	    	
//  	    }
  	    
  	    protected void reCopyDataBase(){
  	    	try {
  				copyDataBase();
  			} catch (IOException e) {
  				// code here
  			}
//  	    	createDatebase();
  	    }
  	    
  	    private boolean checkDataBase() {
  	    	SQLiteDatabase checkDB = null;
  	    	try{
  	    		String fullPath = context.getDatabasePath(dbName).getAbsolutePath();
  	    		Log.e("DataBaseHelper", "fullPath = "+fullPath);
  	    		checkDB = SQLiteDatabase.openDatabase(fullPath, null, SQLiteDatabase.OPEN_READONLY);
  	    	}
  	    	catch (SQLiteException e) { 
  	    		Log.w(getClass().getSimpleName(), e.getMessage());
  	    	}
  	 
  	    	if (checkDB != null) {
  	    		checkDB.close();
  	    		return true;
  	    	}
  	    	else {
  	    		if(isExternalPathExist(context.getDatabasePath(dbName).getAbsolutePath()))
  	    			return true;
  	    		else	
  	    			return false;
  	    	}
  	    }
  	 
  	    private void copyDataBase() throws IOException{
  	    	InputStream is = context.getAssets().open(dbName);
  	    	String outPath = context.getDatabasePath(dbName).getAbsolutePath();
  	    	OutputStream os = new FileOutputStream(outPath);

  	    	byte[] buffer = new byte[1024];
  	    	int length;
  	    	while ((length = is.read(buffer)) > 0) {
  	    		os.write(buffer, 0, length);
  	    	}
  	 
  	    	os.flush();
  	    	os.close();
  	    	is.close();
  	    	
//  	    	createDatebase();
  	    }
  	 
  	    protected void openDataBase() throws SQLException {
  	        String fullPath = context.getDatabasePath(dbName).getAbsolutePath();
  	    	database = SQLiteDatabase.openDatabase(fullPath, null, SQLiteDatabase.OPEN_READWRITE);
  	 
  	    }
  	 
  	    @Override
  		public synchronized void close() {
  	    	if (database != null)
  	    		database.close();
  	 
  	    	super.close();
  		}
  	 
  		@Override
  		public void onCreate(SQLiteDatabase db) {
  			
  		}
  	 
  		@Override
  		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
  	 
  		}
  		
  		public synchronized long insert(String table, ContentValues cv) {
  			try{
  				if (database == null || !database.isOpen()){
  					openDataBase();
  				}
  				return database.insert(table, null, cv);
  			}
  			catch(Exception e){
  				try {
  					createDataBase();
  				} 
  				catch (IOException eCreate) {
  				}
  				openDataBase();
  				
  				return database.insert(table, null, cv);
  			}
  		}
  		
  		public synchronized long update(String table, ContentValues cv, String condition) {
  			try{
  				if (database == null || !database.isOpen()){
  					openDataBase();
  				}
  				return database.update(table, cv, condition, null);
  			}
  			catch(Exception e){
  				try {
  					createDataBase();
  				} 
  				catch (IOException eCreate) {
  				}
  				openDataBase();
  			
  				return database.update(table, cv, condition, null);
  			}	
  		}
  		
  		public synchronized int delete(String table, String condition) {
  			try{
  				if (database == null || !database.isOpen()){
  					openDataBase();
  				}
  				return database.delete(table, condition, null);
  			}
  			catch(Exception e){
  				try {
  					createDataBase();
  				} 
  				catch (IOException eCreate) {
  				}
  				openDataBase();
  			
  				return database.delete(table, condition, null);
  			}		
  		}
  		
  		public synchronized Cursor query(String queryString) {				
  			try{
  				if (database == null || !database.isOpen()){
  					openDataBase();
  				}
  				return database.rawQuery(queryString, null);
  			}
  			catch(Exception e){
  				try {
  					createDataBase();
  				} 
  				catch (IOException eCreate) {
  				}
  				openDataBase();
  			
  				return database.rawQuery(queryString, null);
  			}		
  		}	
  		
  		public void execSQL(String sql){
  			database.execSQL(sql);
  		}
  		
  		public SQLiteDatabase getDatabase() {
  			return database;
  		}
  		
  		public void clearTable(String table) {
  			 database.execSQL("TRUNCATE TABLE " + table);
  		}
  		
  		public void createTableVersion(){
  			database.execSQL("CREATE TABLE [VERSION] ([APP_VERSION] INTEGER  NULL,[DB_VERSION] INTEGER  NULL)");
  		}
  		
  		
  	}
  	
  	
  	
	public static boolean isExternalPathExist(String path) {
		if (path == null) {
			return false;
		} else {
			File f = new File(path);
			if (f.canRead()) {
				return true;
			} else {
				return false;
			}
		}
	}
	
}
